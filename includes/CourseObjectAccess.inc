<?php

/**
 * Access handler for CourseObjects.
 *
 * Subtypes must define take(), see(), and view().
 */
abstract class CourseObjectAccess {

  private $courseObject;
  private $type;

  public function setType($type) {
    $this->type = $type;
  }

  /**
   * Helper method to get possible objects.
   */
  public function getObjectOptions() {
    $options = array('');
    foreach ($this->getCourseObject()->getCourse()->getObjects() as $courseObject) {
      if ($courseObject->getId() != $this->getCourseObject()->getId()) {
        $options[$courseObject->getId()] = $courseObject->getTitle();
      }
    }
    return $options;
  }

  public function setCourseObject($courseObject) {
    $this->courseObject = $courseObject;
  }

  /**
   * Get the course object associated with this access rule.
   *
   * @return CourseObject
   */
  public function getCourseObject() {
    return $this->courseObject;
  }

  /**
   * Can the user take the object?
   *
   * @param type $account
   * @return boolean
   */
  public function take($account) {
    // Block access if conditions evaluate.
    $alter = $this->getOption('alter');
    if ($this->evaluate($account) xor !empty($alter['negate'])) {
      if ($alter['access']) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * Can the user see the object in the outline?
   *
   * @param type $account
   * @return boolean
   */
  public function see($account) {
    return TRUE;
  }

  /**
   * Can the user view the object but not interact?
   *
   * @param type $account
   * @return boolean
   */
  public function view($account) {
    return TRUE;
  }

  public function getOptions() {
    $plugins = $this->getCourseObject()->getOption('plugins');
    if (isset($plugins['access'][$this->type])) {
      return $plugins['access'][$this->type];
    }
    else {
      return $this->optionsDefinition();
    }
  }

  public function getOption($option) {
    $options = $this->getOptions();
    if (isset($options[$option])) {
      return $options[$option];
    }
    else {
      return NULL;
    }
  }

  /**
   * Define the form to be used in the object access settings area.
   */
  public function optionsForm($form, &$form_state) {
    $config = $this->getOptions();

    $form['alter']['#weight'] = 100;
    $form['alter']['#type'] = 'fieldset';
    $form['alter']['#collapsed'] = FALSE;
    $form['alter']['#collapsible'] = FALSE;
    $form['alter']['#title'] = t('Modifications');
    $form['alter']['#attributes']['class'][] = 'course-object-access-alter';

    $form['alter']['negate'] = [
      '#title' => t('If the above condition(s) are:'),
      '#type' => 'select',
      '#options' => [
        0 => t('TRUE'),
        1 => t('FALSE'),
      ],
      '#default_value' => isset($config['alter']['negate']) ? $config['alter']['negate'] : 0,
    ];

    $form['alter']['access'] = [
      '#title' => t('Block access'),
      '#description' => t('This will prevent the user from accessing the course object.'),
      '#type' => 'checkbox',
      '#default_value' => isset($config['alter']['access']) ? $config['alter']['access'] : -1,
    ];

    $form['alter']['required'] = [
      '#title' => t('Required'),
      '#description' => t('Change the "Required" property.'),
      '#type' => 'radios',
      '#options' => ['-1' => t('No change'), 1 => t('Make required'), 0 => t('Make optional')],
      '#default_value' => isset($config['alter']['required']) ? $config['alter']['required'] : -1,
    ];

    $form['alter']['visible'] = [
      '#title' => t('Visibility'),
      '#description' => t('Change the "Visible" property.'),
      '#type' => 'radios',
      '#options' => ['-1' => t('No change'), 1 => t('Visible'), 0 => t('Hidden')],
      '#default_value' => isset($config['alter']['visible']) ? $config['alter']['visible'] : -1,
    ];

    return $form;
  }

  /**
   * Validate the access form.
   */
  public function optionsValidate($form, &$form_state) {

  }

  /**
   * Submit the access form.
   */
  public function optionsSubmit($form, &$form_state) {

  }

  /**
   * Define access settings.
   */
  public function optionsDefinition() {
    return array();
  }

  /**
   * Alter a property during display/tracking. Does not affect the stored
   * options. This is called in CourseObject::getReadOnlyOptions().
   *
   * @param array $property
   *   Array of object options.
   *
   * @param object $account
   *   User account to evaluate.
   */
  public function alterOptions(&$options, $account) {
    $alter = $this->getOption('alter');
    if ($this->evaluate($account) xor !empty($alter['negate'])) {
      // Conditions passed. Apply modifications.
      if (isset($alter['required']) && $alter['required'] != -1) {
        $options['required'] = $alter['required'];
      }

      if (isset($alter['visible']) && $alter['visible'] != -1) {
        $options['hidden'] = !$alter['visible'];
      }
    }
  }

  public function evaluate($account) {

  }

}
