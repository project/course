<?php

/**
 * @file course blocks
 */

/**
 * Course block callback: define outline view block.
 */
function _course_block_outline_view() {
  if ($node = course_get_context()) {
    if ($output = course_outline_list($node)) {
      return array('subject' => t('Course outline'), 'content' => $output);
    }
  }
}

/**
 * Course block callback: navigation view.
 */
function _course_block_navigation_view() {
  global $user;
  $node = course_get_context();
  if ($node) {
    $course = course_get_course($node);
    $links = $course->getNavigation();

    $items = array();

    $active_object = $course->getActive();
    $next_object = $course->getNext();

    if (isset($active_object) && !$active_object->getFulfillment($user)->isComplete() && $active_object->hasPolling() && !isset($links['next'])) {
      // Add a fake next button that will fire an AJAX check on fulfillment.
      ctools_include('ajax');
      $ajax_dest = 'node/' . $active_object->getCourseNid() . '/course-object/' . $active_object->getId() . '/nojs/nav';
      $links['check'] = ctools_ajax_text_button(t('Next'), $ajax_dest,t('Next'));
    }

    foreach ($links as $key => $value) {
      $items[] = array(
        'class' => array('course-nav-' . $key),
        'data' => $value,
      );
    }

    // Attach js/css for navigation popup.
    $block['nav_popup']['#attached']['library'][] = array('system', 'drupal.ajax');
    $block['nav_popup']['#attached']['library'][] = array('system', 'ui.dialog');
    $block['nav_popup']['#attached']['js'][] = drupal_get_path('module', 'course') . '/js/nav.js';
    $block['nav_popup']['#attached']['css'][] = drupal_get_path('module', 'course') . '/css/nav.css';
    $block['nav_popup']['#markup'] = '<div id="course-nav-popup" title="Warning"><p>You are not yet complete.</p></div>';

    $block['nav'] = [
      '#theme' => 'item_list',
      '#items' => $items,
      '#title' => '',
      '#type' => 'ul',
      '#attributes' => array('id' => 'course-nav'),
    ];

    return array(
      'subject' => '',
      'content' => $block,
    );
  }
}
