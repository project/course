(function ($) {
  Drupal.ajax.prototype.commands.course_nav_popup = function () {
    // Pop up the "waiting for completion" dialog.
    $("#course-nav-popup").dialog({
      draggable: false,
      modal: true
    });
  };

  Drupal.ajax.prototype.commands.course_nav_next = function () {
    // Auto-navigate to the next object or completion page.
    $(location).attr("href", $("li.course-nav-next a").attr("href"));
  };
}(jQuery));
